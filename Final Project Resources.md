[TOC]

# Final Project Resources

<img src="https://images.unsplash.com/photo-1426927308491-6380b6a9936f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80" style="zoom:67%;" />

I have put together this collection of resources to support your work on your final projects. As you identify things that you find useful and that you'd like to share with your classmates, please send them along and I will add them to this page.



## Twine Resources

![](https://www.sfwa.org/wp-content/uploads/2017/04/twine4.png)

[Twine](https://twinery.org/) is software that, according to its website: "is an open-source tool for telling interactive, nonlinear stories." Some of you played with Twine earlier in the semester to make interactive presentations. Here are some resources to support you as you build great things with it.

Here are some resources to support your adventures in Twine.

1. [A Quick Twine (2.2+) Tutorial](https://catn.decontextualize.com/twine/). This is a really good place to start as it gives a really good overview of how Twine works and how to work it.
2. [Twine Reference](https://twinery.org/reference/en/). This thorough guide put together by the folks at Twine, is very extensive and detailed. If you are new to Twine, I suggest started with the [Getting Started section](https://twinery.org/reference/en/getting-started/index.html).
3. [Twine Cookbook](https://twinery.org/cookbook/). This *Cookbook* contains documentation, tips, and examples for using Twine.
4. [Create Your Own Digital Adventure Gamebook with Twine](https://www.dropbox.com/s/x48u2vhbff7biqb/MagPi64TWINE.pdf?dl=0). This article, which begins on page 62, will walk you through creating a digital adventure game with Twine (just like the title says).

It is worth noting that there are there are several [story formats](http://twinery.org/cookbook/introduction/story_formats.html), which you can think of like *flavors* of Twine. Two of them, **Snowman** and **Chapbook** use Markdown.

***



## Markdown

![](https://th.bing.com/th/id/R.5a4668debd4abaabf187eb03594ce57d?rik=32D7GtcoCkR%2bmQ&riu=http%3a%2f%2frmarkdown.rstudio.com%2fimages%2fquicktourExample.png&ehk=Zw252yYQVKM%2bMz1Wox41%2brNPnCBfZtPuyEXOSjZ%2bPfI%3d&risl=&pid=ImgRaw&r=0)

As you already know, [Markdown](https://en.wikipedia.org/wiki/Markdown) is an easy to use markup language. Markdown can be used to easily create beautiful multimedia documents for all kinds of purposes. 

### Markdown Editors

There are many tools called ***Markdown Editors*** which can be used to create Markdown documents. I even have one (Markor) on my phone. We have mostly used [StackEdit](https://stackedit.io/) in class so far. StackEdit is good, but has limitations. Here are some others you may want to try. 

1. [HackMD](https://hackmd.io). Like StackEdit, HackMD is a web-based Markdown editors. It is far more robust than StackEdit, and also has features like managing multiple documents and collaboration. 
2. [Editor.md](http://editor.md.ipandao.com/en.html). Editor.md is an interesting open-source web-based Markdown editor that you can also embed on your own web pages if you want.
3. [Typora](https://typora.io). Typora is a lightweight, but powerful, Markdown editor. It is the tool I have used all semester to create documents like this one as well as posts to Moodle. There are versions for all major computer operating systems (Mac, Windows, LInux)
4. [Remarkable](https://remarkableapp.github.io). Remarkable is a really good Markdown editor for Windows or Linux computers.
5. [MacDown](https://macdown.uranusjr.com). As you might guess, MacDown is a Markdown editor for the Mac.

### Markdown Resources
Here are some resources to support you in leveling up your Markdown skills.
1. [HackMD Features](https://hackmd.io/s/features). This site includes all kinds of features contained in HackMD.
2. [HackMD Extra Support Syntax](https://hackmd.io/c/codimd-documentation/%2F@codimd%2Fextra-supported-syntax). A deep, deep dive into HackMD.
3. [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/). A quick guide to using Markdown powerfully.
4. [Markdown Guide](https://www.markdownguide.org). A very extensive (and scaffolded) reference for working with Markdown.
5. [Creating a Slidedeck in HackMD](https://hackmd.io/s/how-to-create-slide-deck). One of my favorite things to do with Markdown is to create slideshows. Here's how to do so.

***

## Data Analysis and Visualization 

![](https://opendatascience.com/wp-content/uploads/2022/01/charts-g4e68a6064_640-640x350.png)

You have done a really good job exploring data analysis and visualization using spreadsheet tools (like **Excel** and **Google Sheets**), as well as other tools (like [Datawrapper](https://www.datawrapper.de) and [RawGraphs]()).



#### Data Analysis and Visualization Resources

Here are some resources to support your continued work with these tools.
1. [Analyze Data in Excel](https://support.microsoft.com/en-us/office/analyze-data-in-excel-3223aab8-f543-4fda-85ed-76bb0295ffc4). This web site has a collection of resources for performing data analysis and data visualization using Excel. 
2. [Analyze Data in Sheets](https://support.google.com/docs/answer/9330962?hl=en). This web site has a collection of resources for learning to perform data analysis and data visualization using Google Sheets.
3. [Datawrapper Academy](https://academy.datawrapper.de). This is an extensive set of useful tutorials for everything Datawrapper.
4. [RawGraphs Tutorials](https://www.rawgraphs.io/learning). This is an extensive set of useful tutorials for everythign RawGraph.
5. [R for Data Science](https://r4ds.had.co.nz/introduction.html). This is a terrific book (online and free) that walks you through how to perform data analysis and data visualization using the [R programming language](https://www.r-project.org/about.html).
6.[Complete Guide to Data Visualization with Python](https://towardsdatascience.com/complete-guide-to-data-visualization-with-python-2dd74df12b5e). A great overview of performing data analysis and data visualization using the ]Python programming language](https://www.python.org).