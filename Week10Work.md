[TOC]

## Here is the work for Week 10 of our course.

<img src="https://miro.medium.com/max/1400/1*SoiFFdigLZP05BaZpehnAg.png" style="zoom:67%;" />

***


### Data and Data Visualization Project
This week we begin our two week adventure into dealing with data and data visualization. To guide this adventure, I have created a project. You can find the [details here](https://docs.google.com/document/d/1dMKvc-0oWLxq63nk00wG0sooSRHGHijY3JWWHt6twxY/edit?usp=sharing).

In this project you will:   
1. **Choose** some data to explore  
2. **Explore** those data using various software tools;  
3. **Make visualizations/tell stories** with/of those data  
4. **Reflect** on your experiences working with data and data visualizations.  

As you will see, the project is organized into **2 chunks**. You will complete the first chunk this week.

#### Which Tools?
<img src="https://images.unsplash.com/photo-1508873535684-277a3cbcc4e8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" style="zoom:67%;" />

If you are someone who **does not** feel confident with making charts in [**Excel**](https://www.microsoft.com/en-us/microsoft-365/excel) or [**Google Sheets**](https://www.google.com/sheets/about/), you should start this project with one of those tools.
If you are someone who **does** feel confident in making charts with Excel or Google Sheets, you should start with one of the more advanced tools, such as [**Datawrapper**](https://www.datawrapper.de) or [**RawGraphs**](https://app.rawgraphs.io). 
If there are other tools you wish to use (such as Python or R), you are free to do so.

Resources and tutorials for each are included in the project requirements

I am available at any time to work with you on your projects.

***

***You should complete Chunk 1 of the Data and Data Visualization Project by our next class on November 15.***


***



