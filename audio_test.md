* **Embedding Sounds in Markdown Documents**

  * It is pretty straightforward to embed audio into Markdown documents. To do this, you would borrow the <audio> tag from HTML. Here is an example and the associated code.


```html
<audio controls>
  <source src="https://bigsoundbank.com/UPLOAD/wav/0004.wav" type="audio/wav" />
</audio>
```

<audio controls>
  <source src="https://bigsoundbank.com/UPLOAD/wav/0004.wav" type="audio/wav" />
</audio>

The key parts are the link to the sound file. Here is a [great, free repository](https://bigsoundbank.com) of all kinds of sound files. The file  must end in a sound related extension (like wav, ogg, mp3). Then, you would update the "type" section to reflect the type of file you are using.
