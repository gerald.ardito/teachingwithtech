## Work for Week 1

<img src="https://images.unsplash.com/photo-1491309055486-24ae511c15c7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" alt="reading a book" width="600"/>

Here is the work for Week 1 of our course.
By the way, here are the [notes](https://gitlab.com/gerald.ardito/teachingwithtech/-/blob/main/Week1Notes.Rmd) I created from our first class. Start this week's work by reviewing them. Be sure to let me know if you have any edits to suggest.

***

### The Big Picture
For our first week's work, we are going to do two things. First, I am asking that you **read and respond** to two texts.
Then, I am going to ask you to **capture and document** the tools you assembled to make that happen.

***

### Sharing your Work
For those of you with using the  Little Freedom option, please create a Google Doc or Office 365 doc which you will share with me. For a **Google doc**, use *arditog@mville.edu*. For an **Office 365 doc**, use *gerald.ardito@mville.edu*.  Be sure to give me editing privileges. Include both your responses to the readings and whatever visualizations you create to capture the tools you have assembled in whatever document you share.

For those of you using the Lots of Freedom option, put together your responses to the readings and whatever visualizations you create to capture the tools you have assembled, and then email me to let me know where I can access your work.

***

### The Readings
Please read both of these two texts:
1. Bates, T. (2015). [Teaching in a digital age (Open Textbook)](https://pressbooks.bccampus.ca/teachinginadigitalagev2/chapter/section-8-1-a-short-history-of-educational-technology/). Read Chapter 7 ,section 7.2 
2. Dron, J. (2022). [*Educational technology: what it is and how it works*](https://link.springer.com/epdf/10.1007/s00146-021-01195-z?sharing_token=ulLYryekoNv8WpzwtO-4xfe4RwlQNchNByi7wbcMAY7XnEGhz-kpjfkrS7gmR5C1sGlnMu6coqhVRdN_GO4HmvvOX224Ulc7crEX8DF5UA-dFjIQOkUbM2GXsLmZgV3V8T4m-2ajeKhCaB89xnI4ORDWoYoMq8zLei-uazdo5zM%3D). AI & SOCIETY, 37(1), 155-166.

***

### Your Responses
Your responses should make specific connections to the texts and be substantive. Come to our next class ready to discuss the texts and your responses and any insights you derived from them.


***


### ***This work is due by our next class on Tuesday, September 13.***