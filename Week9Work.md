[TOC]

## Here is the work for Week 9 of our course.

<img src="https://i.imgur.com/GRGhUCk.png" style="zoom:67%;" />

***


### Exploring Computational Thinking with Turtle Art, part 2
<img src="https://images.unsplash.com/photo-1465310477141-6fb93167a273?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80&quot;" style="zoom:67%;" />



Your first round of work in Turtle Art was so interesting, inventive, and fun. So, your task for this week is to share what you have (see details below) and then work to deepen or expand what you have done. As I said, last week, the goals of this exploration and play is both to developing some experience in the key ideas of ***computational thinking*** (*decomposition*, *pattern recognition*, *abstraction*, and algorithmic design) as well as in the pedagogical strategy I have dubbed ***MAD*** - *minimally assisted discovery*.

Here are the details for your second round of Turtle Art adventures.

1. **Save and Share Your Turtle Art Creations **. Please save and share the TurtleArt projects you shared with us in class. As we discussed, TurtleArt allows you to save your work as a PNG file, which also cleverly contains the blocks (your code) as well. Here is a screenshot from the [TurtleArt Reference](https://www.playfulinvention.com/webta/turtleart-v3/webhelp/reference.html) that demonstrates how to do this. Please do this by **the end of the day on Wednesday, November 2nd.**

   ![](https://i.imgur.com/R6NYWr5.png)

2. **Go Bigger/Go Deeper**. You all mentioned in your class presentations what you might want to try next and/or that you were inspired by the work of others. For our next class, please be ready to show off what you did to go bigger and/or deeper in TurtleArt. You may wish to refer to the class recording for reminders, inspiration, etc.. For other inspiration, you may want to check out the resources below.


#### Turtle Art Resources
Here are some resources that can support your explorations and play.

1. [**Turtle Art Reference Guide**](https://www.playfulinvention.com/webta/turtleart-v3/webhelp/reference.html) This is a resource put together by the developers of [Turtle Art](https://www.playfulinvention.com/webta/turtleart-v3/).

1. [**Turtle Art Samples**](https://www.playfulinvention.com/webta/turtleart-v3/webhelp/samples.html) This a collection of samples provided by the Turtle Art folks.

1. [**Artemis Papert/Bryan Silverman Art Book**](https://www.blurb.com/books/1592450-turtle-art) Artemis Papert (Seymour's daughter) and Brian Silverman(the developer of Turtle Art, and an original developer on Scratch and other LOGO descendants) have put together this book of art created with Turtle Art. As you page through it, you can see the art and the associated Turtle Art code. Note - you can page through the book by clicking on it.

   

***

### Playing with Art Logo



![](https://i.imgur.com/piUokU0.png)

In class, I introduced you to the version of LOGO developed by the folks who developed TurtleArt. It is called ArtLogo and you can find it [here](http://playfulinvention.com/artlogo/). 

Your task this week is to play around with ArtLogo, using your own imagination and the resources listed below and do something that you think is cool. You may also want to try out some of the projects described in the [20 Things paper](https://www.dropbox.com/s/iorvltfy3ejn0ka/20%20Things%20AIM-248.pdf?dl=0).



#### ArtLogo Resources
Here are some resources to help you get started with ArtLogo.
1. [ArtLogo Tips](http://playfulinvention.com/artlogo-help/tips.html#procs) This is a good starting place for working with ArtLogo.
2. [ArtLogo Reference](http://playfulinvention.com/artlogo-help/prims.html). This is a far more extensive reference for ArtLogo.
3. [ArtLogo Secrets](https://docs.google.com/document/d/1CR3TDGYkEhp6hvN3DEjh8Nf5sTGxV2O5En6ck3sQOew/preview). This is a document that contains things not contained in either the Tips or Reference documents, most importantly how to save and load ArtLogo code.
4. [ArtLogo Examples](https://www.dropbox.com/sh/xrmlazaj520a7eg/AADrD_StGvJXkbShEJQg3Dlja?dl=0). Here are the files from the couple of examples I demonstrated in class.

***

***This work is due by our next class on November 8.***


***



