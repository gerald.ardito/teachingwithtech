[TOC]

## Here is the work for Weeks 5/6 of our course.

<img src="https://images.unsplash.com/photo-1588201689891-36f098ccee1b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80" style="zoom:60%;"/>

In this week's work, we will finalize the design of our learning pathways, as we discussed in class.

***

### Designing a Learning Pathway, Part 2
This week, you will finalize the design of your learning pathway. 

<img src="https://images.unsplash.com/photo-1625434132728-97518a6a3c86?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1173&q=80" style="zoom:50%;" />

Here is the overall idea.

You have already begun creating a learning pathway that supports someone else's learning. Think about someone spending about 90 minutes learning what you want them to learn. 

As a reminder, here is what there is to take into account (along with the feedback you received in class from your colleagues):

1. **What do you want someone to learn? ** Is it *knowledge* (like the Battle of Long Island), a *skill* (how to shoot a 3-point shot in basketball), or an *emotional experience* (how to love the music of Beethoven).
2. **What kind of learning do you want to accomplish?** Understanding, performance, or experience.
3. **Who is your learner?** Are they a novice, intermediate, or experienced learner in whatever it is you want to teach them? (By the way, if you use a [choice board](https://www.edutopia.org/article/using-choice-boards-boost-student-engagement) type approach, you can let the learner decide his/her/their level).
4. **How will they learn?** What kind of pathway(s) would be most effective given the who and the what and the how you are planning. 
5. **What technologies and tools are best for the design and implementation and sharing of this learning pathway?** Here are the examples I shared during class: [Heritage Month Choice Board](https://docs.google.com/document/d/1q2dCFCbQC4UAn3Ontqw9FeLOPOu9jmtmTrekFagwwJI/edit?usp=sharing)) and [Mendelian Genetics HyperDoc](https://docs.google.com/document/d/1B0bpPhophESHyA_5giH_D6jpjBY9qObnmZgL4Lz5HRw/edit?usp=sharing). I share these as a starting point only. I invite and encourage you to develop whatever you think is best. The choice is entirely up to you.

#### Sharing Your Work
For the final version of your work, please share whatever materials you have put together with me. If you created a collaborative document, please share it with me and be sure to give me editing privileges. 

For Google Docs, use: arditog@mville.edu

For Microsoft 365, use: gerald.ardito@mville.edu

Or, you can email me a link to whatever else you used to create and share your learning pathway.

***Be prepared to share your work with the class during our next session on Tuesday, October 18***

### ***This work is due by our next class on October 18.***


***



