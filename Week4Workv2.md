[TOC]

## Here is the work for Week 4 of our course.

<img src="https://images.unsplash.com/photo-1478860409698-8707f313ee8b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=800" style="zoom:60%;"/>

This week, we begin the design of our learning pathways, as we discussed in class.

***

### Sharing Your Final (for now, at least) Markdown Work from StackEdit

<img src="https://i.stack.imgur.com/0RDUQ.png" style="zoom:45%;"/>



If you have not already done so, please share the second/final round of work you did in Markdown. As before, please export a Markdown file from StackEdit and email it to me. 

I am working on the open items you raised in your presentations (working with Tables of Contents, embedding sounds, displaying quizzes, etc. As I figure things out, I will share my solutions with you.)

**Please do this by Thursday, September 29**.



***


### Designing a Learning Pathway, Part 1
This week, you will start designing a learning pathway.
<img src="https://images.unsplash.com/photo-1478860409698-8707f313ee8b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" style="zoom:50%;" />

Here is the overall idea.

You are creating a learning pathway that supports someone else's learning. Here are some things to consider as you get started:

1. **What do you want someone to learn? ** Is it *knowledge* (like the Battle of Long Island), a *skill* (how to shoot a 3-point shot in basketball), or an *emotional experience* (how to love the music of Beethoven).
2. **What kind of learning do you want to accomplish?** Understanding, performance, or experience.
3. **Who is your learner?** Are they a novice, intermediate, or experienced learner in whatever it is you want to teach them? (By the way, if you use a [choice board](https://www.edutopia.org/article/using-choice-boards-boost-student-engagement) type approach, you can let the learner decide his/her/their level).
4. **How will they learn?** What kind of pathway(s) would be most effective given the who and the what and the how you are planning. 
5. **What technologies and tools are best for the design and implementation and sharing of this learning pathway?** Here are the examples I shared during class: [Heritage Month Choice Board](https://docs.google.com/document/d/1q2dCFCbQC4UAn3Ontqw9FeLOPOu9jmtmTrekFagwwJI/edit?usp=sharing)) and [Mendelian Genetics HyperDoc](https://docs.google.com/document/d/1B0bpPhophESHyA_5giH_D6jpjBY9qObnmZgL4Lz5HRw/edit?usp=sharing). I share these as a starting point only. I invite and encourage you to develop whatever you think is best. The choice is entirely up to you.

### ***This work is due by our next class on October 4.***


***



