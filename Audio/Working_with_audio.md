# Working with Audio Files

![Audio recording](https://images.unsplash.com/photo-1478737270239-2f02b77fc618?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80)

## Description
This page is all about working with **audio files** - creating them, converting them from one format to another, editing them, sharing them/embedding them.

## Working with Audio

### Recording Audio

### Editing Audio

### Saving/Converting Audio Files

### Sharing Audio Recordings
