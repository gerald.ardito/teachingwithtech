[TOC]

## Here is the work for Week 8 of our course.

<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRPOnsu8ge0ACwuUKNZp_QDYuKVS71kogrMY24WE-I04RA__wEIRj7pYhc6T2716PlRXQY&usqp=CAU" style="zoom:150%;" />

***


### Exploring Computational Thinking with Turtle Art, part 1
![](https://images.unsplash.com/photo-1603714196939-6f6436c8d0c5?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" )



This week, we will continuing exploring and playing with [Turtle Art](https://www.playfulinvention.com/webta/turtleart-v3). The goals of this exploration and play is both to developing some experience in the key ideas of ***computational thinking*** (*decomposition*, *pattern recognition*, *abstraction*, and algorithmic design) as well as in the pedagogical strategy I have dubbed ***MAD*** - *minimally assisted discovery*.

Your task this week is a bit of a build your own adventure. Regardless of the branch(es) your choose or invent, your goal is to come back and wow us with what you have built with Turtle Art. 

You can:

1. **Continuing the Explorations into Turtle Art**. In this branch, you can follow your own interests and curiosity and wondering (as you did last night) and come back and show us what you have done (and where you have been).
2. **Write Your Name**. In this branch, you will use Turtle Art to write your name in block letters, upper and lower case letters, or script (or any combination thereof).
3. **Build Something Else**. In this branch, you can come up something else to explore in Turtle Art entirely.


### Turtle Art Resources
Here are some resources that can support your explorations and play.

1. [**Turtle Art Reference Guide**](https://www.playfulinvention.com/webta/turtleart-v3/webhelp/reference.html) This is a resource put together by the developers of [Turtle Art](https://www.playfulinvention.com/webta/turtleart-v3/).
1. [**Turtle Art Samples**](https://www.playfulinvention.com/webta/turtleart-v3/webhelp/samples.html) This a collection of samples provided by the Turtle Art folks.
1. [**Artemis Papert/Bryan Silverman Art Book**](https://www.blurb.com/books/1592450-turtle-art) Artemis Papert (Seymour's daughter) and Brian Silverman(the developer of Turtle Art, and an original developer on Scratch and other LOGO descendants) have put together this book of art created with Turtle Art. As you page through it, you can see the art and the associated Turtle Art code. Note - you can page through the book by clicking on it.
1. [**Introduction to Turtle Art Video]. This is a short (under 3 minutes) video introducing the basics of Turtle Art.

<iframe width="560" height="315" src="https://www.youtube.com/embed/qvPDsRl9wA4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



If you come across any other useful resources, please let me know and I will add them to the list.

Have fun! Play hard!


***This work is due by our next class on November 1.***


***



