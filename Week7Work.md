[TOC]

## Here is the work for Week 7 of our course.

<img src="https://images.unsplash.com/photo-1546776310-eef45dd6d63c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=810&q=80" style="zoom:60%;"/>

***
### Sharing and Reflection on Your Final Learning Pathways

<img src="https://images.unsplash.com/photo-1588201689891-36f098ccee1b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80" style="zoom:60%;"/>

Now that you have finished designing and presenting your ***Final Learning Pathways***, it is time to share your final (for now at least) products and reflect on the process of creating them. 

Please share your final products for me via email. You can send links to shared documents or attach files.

Please use [this document](https://gpa-etherpad.us.reclaim.cloud/p/LearningPathwaysReflection) to share your reflections. I have left a spot for each of you to do so here. You do not need to log into anything to collaborate. Just start typing. I have suggested some prompts in the document, but feel free to go beyond them to capture your experiences.

### Playing with Computational Thinking, Part 1
This week, you will get to explore some of the key ideas and foundation work in having children work with computer programming.



<img src="https://logothings.github.io/logothings/images/cyn.jpg" style="zoom:100%;" />



In the late 1960s and early 1970s, some pioneering work was done in learning to have children work with computers by Seymour Papert, along with his MIT colleagues Cynthia Solomon (pictured above) and Marvin Minsky. This week, we will read two seminal papers co-written by Papert and Solomon, and watch a presentation given by Solomon at Manhattanville in October 2021. To bring us powerfully into that world, please read and watch the following.

**Texts**   

1. Papert, S., & Solomon, C. (1971). [Twenty Things to Do With a Computer. Artificial Intelligence Memo Number 248](https://drive.google.com/file/d/1000ndfXe-eQ9G_XNZvXH7w7Hz8uqsAh7/view?usp=sharing).  

2. Papert, S. (1971). [Teaching children thinking (LOGO memo)](https://drive.google.com/file/d/1eEPd1aceN61r_1kNWIJKFRc0cXJnyCmm/view?usp=sharing).  

**Video**

<iframe width="560" height="315" src="https://www.youtube.com/embed/dtzyGAMhIeE?start=2040" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

You need only watch Cynthia Solomon's presentation.

Come to our next class ready to discuss these texts and video.

***This work is due by our next class on October 25.***


***



