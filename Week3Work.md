## Here is the work for Week 3 of our course.

<img src="https://images.unsplash.com/photo-1617118602031-1edde7582212?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" style="zoom:67%;"/>


***

### Sharing and Continuing Your Markdown Work from StackEdit

<img src="https://i.stack.imgur.com/0RDUQ.png" style="zoom:45%;"/>

You have two tasks this week regarding working with [Markdown](https://www.markdownguide.org)  in [StackEdit](https://stackedit.io).


1. Share the work you did for our first round (and that you presented in our last class). To do this, you will export a Markdown file from StackEdit and then email it to me. Here is a brief (less than 90 seconds) video that walks you through the process.  **Please do this by Thursday, September 22**.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XktZnxMSZBU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

2. Continue to deepen your Markdown/StackEdit skills. I am in the midst of compiling resources based on what you shared in your class presentations. Once I have this finalized, I will share it with you. :construction_worker:    Come to our next class ready to show off your *final* Markdown projects.



***

### Self Assessment
This week you will be creating and sharing your self assessment of you skills using a wide set of learning technologies. 

To do this, I ask that you use [this template](https://docs.google.com/document/d/19N1zoYKz6sqbM1Ej9SCbhgbEJOQEg7Iv8eBFTzmgcfI/edit?usp=sharing). In Google Docs, make a copy and then share with with me (including editing privileges) at: arditog@mville.edu. Please include your name in the title of the document.

As we discussed, the task is to complete a self assessment using the categories provided. How you do it is entirely up to you. In the past, some students have done this narratively. Others have created a rating scale and then scored themselves. This is entirely up to you.

### ***This work is due by our next class on Tuesday, September 27.***


***



