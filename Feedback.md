## Feedback of Various Kinds

We have been reading this week about the various ways that teachers can use feedback effectively to support the learning and development of our students. I wanted to make transparent the ways that I have been using feedback in our course so far.



### Email

Each week, I send an email announcing the work for the following week, as you know.  I try to include some kind of general feedback in those emails. My goal is this type of feedback is to respond generally and publicly to our collective work.

Here is an example of this type of feedback.

![](https://i.imgur.com/aE8YYwu.png)



### Journal Entries - Individual Feedback

As you know, we are using collaborative writing tools, Google Docs and Office 365 - so that I can leave feedback for you using the *comments* features.

This feedback is intended to be both personal (it is only addressed to you) and specific (it is designed to either improve your writing and/or deepen and expand your thinking). 

Here (with Jillian's permission) is an example of this type of feedback.

![](https://i.imgur.com/I5YKDzb.png)



As I have said in other places (emails and my weekly audio messages), these are for me conversations in which both of us can engage. Therefore, this is an **open** and **bi-directional type** of feedback.

## Mattermost Feedback

### Modeling Feedback

For this course, we are using [Mattermost](https://mattermost.com) as a container for our class discussions. As I have mentioned elsewhere, I have chosen this type of chat based tool in order to have discussions that seem less formal or rigid than Discussion Boards in a tool like Blackboard.

As a participant in these discussions, I try to participate as authentically as I can by asking questions, suggesting connections, etc.. This is more of a *modeling* use of feedback. 

Here is an example of this *modeling*.

![](https://i.imgur.com/Qx6Vv78.png)

### Class Level Feedback
Something that I have been doing each week with your Mattermost discussions (and that I have not yet shared with you), is to use [*textual or content analysis*](https://en.wikipedia.org/wiki/Content_analysis) tools to explore our discussions to look for patterns, trends, themes, etc.. This is also something I used to do when I had middle school students as well.

Let's take the Mattermost discussion for Guiding Question #3  as an example. That question was:

> What roles do trust, respect, and autonomy/agency play in a learning environment? How have you or how have you seen other teachers orchestrate these things (either well or badly)?

When I perform this textual analysis on our discussion, this *WordCloud* pops out:

![](https://i.imgur.com/rdeeEt9.png)

The size of the words in this cloud are determined by their frequencies, so in this discussion the most used words are: *students*, *trust*, and *respect*. I encourage you to look at this visualization to see what other patterns you see and share them with us (in Mattermost, of course).

[By the way, I used a free, web-based too called [Voyant-Tools](https://voyant-tools.org) for this type of analysis.]

Another tool in the Voyant arsenal is called *Links* which shows the connections between words in the text being analyzed. Here is a visualization of the connections from our Guiding Question 3 discussion.



![](https://i.imgur.com/IpOg3Iy.gif)

You can also play with these data live [here](https://voyant-tools.org/?corpus=78c53f8e93326a175a7b7c36a3006641&view=CollocatesGraph).

I find this type of visualization useful because it goes beyond simple word frequencies to look at which words were used near one another in the Mattermost discussions. What kinds of patterns do you see?

This type of feedback is about allowing students (and me, too) to discern patterns in ways that hopefully deepen understanding and provoke further questions.

I invite your responses to this *essay* on feedback.