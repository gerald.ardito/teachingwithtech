[TOC]

## Here is the work for Week 11 of our course.

<img src="https://opendatascience.com/wp-content/uploads/2022/01/charts-g4e68a6064_640-640x350.png" style="zoom:100%;" />

***


### Data and Data Visualization Project, Chunk 2
This week we continue our two week adventure into dealing with data and data visualization. We are going to dive into Chunk 2 of our project. You can find the [details here](https://docs.google.com/document/d/1dMKvc-0oWLxq63nk00wG0sooSRHGHijY3JWWHt6twxY/edit?usp=sharing).

As you now know, in this project you will:   
1. **Choose** some data to explore  
2. **Explore** those data using various software tools;  
3. **Make visualizations/tell stories** with/of those data  
4. **Reflect** on your experiences working with data and data visualizations.  

As you will see, the job in **Chunk 2** is to deepen and expand your data analysis and data visualization skills. 

#### Which Tools?
<img src="https://images.unsplash.com/photo-1501516069922-a9982bd6f3bd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" style="zoom:57%;" />

How you proceed in **Chunk 2** is up to you. Can you can continue to explore working  with making charts in [**Excel**](https://www.microsoft.com/en-us/microsoft-365/excel) or [**Google Sheets**](https://www.google.com/sheets/about/), or move onto/continue to work with the more advanced/specialized tools, such as [**Datawrapper**](https://www.datawrapper.de) or [**RawGraphs**](https://app.rawgraphs.io). 

As I said last week, if there are other tools you wish to use (such as Python or R), you are free to do so.

Resources and tutorials for each are included in the project requirements

I am available ***at any time*** to work with you on your projects.

***
### Designing a Final Project
<img src="https://images.unsplash.com/photo-1581490278090-2249a0204d95?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80" style="zoom:57%;" />

The last chunk of work for this course will be a **final project**. 
In this project you can either choose something we have already worked on so far this semester and dive more deeply into it **OR** you can choose another area of learning technologies that you would like to explore for 2-3 weeks.

You can see the project requirements and rubric [here](https://docs.google.com/document/d/1XPaE36CtgFi5w_ARRhI4VbAsYSrfegOy-Hi0TdMMl58/edit?usp=sharing).

As you will see, your first task is to create a project proposal, which I will approve. Please do this via a shareable document, such as Google Docs or Office 365. If Google Docs, share via arditog@mville.edu. If Office 365, share via gerald.ardito@mville. In either case, please be sure to give me editing privileges.

Your proposal is due by ***our next class on November 22***.


***

You should complete Chunk 2 of the Data and Data Visualization Project and your Final Project Proposal by our next class on November 22.***


***



