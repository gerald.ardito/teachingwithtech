## Markdown Resources

<img src="https://th.bing.com/th/id/R.5a4668debd4abaabf187eb03594ce57d?rik=32D7GtcoCkR%2bmQ&riu=http%3a%2f%2frmarkdown.rstudio.com%2fimages%2fquicktourExample.png&ehk=Zw252yYQVKM%2bMz1Wox41%2brNPnCBfZtPuyEXOSjZ%2bPfI%3d&risl=&pid=ImgRaw&r=0" style="zoom:50%;" />  

1. [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/). The basics of Markdown in one place.  
2. [Markdown Basic Syntax](https://www.markdownguide.org/basic-syntax/). This is a more detailed description/explanation of how to work with Markdown.

#### Videos
<img src="https://freesvg.org/img/1381070386.png" alt="video icon" width="300"/>

I have created two videos to help you work with writing Markdown in StackEdit.  
1. [Introduction to StackEdit](https://youtu.be/UPjXcp7981Y)  
2. [Sharing and Publishing in StackEdit](https://youtu.be/e57i0qaqL6o) 

#### Examples
[Here](https://gitlab.com/gerald.ardito/teachingwithtech/-/tree/main/Markdown%20Samples) is where you can examples of Markdown created by former students and myself.

#### Other Markdown Goodies

* [**Emojis** in Markdown](https://gist.github.com/rxaviers/7360908)
* **Embedding Videos in Markdown**. In order to embed a YouTube video in a Markdown document, grab the embed code from YouTube (you get it from the ***Share** menu). Then paste that into your Markdown document. ![](https://i.imgur.com/5Wrw5NR.png)
* **Working with Images in Markdown**
  * [Scaling images in Markdown](https://www.educative.io/answers/how-to-change-image-size-in-markdown) 
  * [Aligning images in Markdown](https://davidwells.io/snippets/how-to-align-images-in-markdown)

* **Mermaid**
  * [User documentation on Mermaid](https://mermaid-js.github.io/mermaid/#/). This contains everything there is to know about using Mermaid, including formatting, types of diagrams, and lots of other goodies.
  * [Mermaid Live Editor](https://mermaid.live/edit#pako:eNplkLEOwjAMRH_F8twFIZasVGLKxNrFagwEmhiljlCF-HfSlkoFPFnv7DvZT2zFMRoEuEpOkYcmQin12jHYAR6Sbj6ewdFH6blVLxEOAiqTPHMASzcGZTKwM2B5wWUw33sln3oD27VSz_sGNiOtYE_6l3GRsHZy8oiL11fK0esk_kRghYFTIO_Kjc-RNqgXLp5oSuv4RLnTBpv4KqOUVY5DbNFoylxhvjtSrj2dEwU0J-r6Qtl5lWTnv03ve70Btclk-w). This is a place to just create Mermaid diagrams of various kinds. It then generates code you can add to your Markdown document.
  * Formatting/styles ([themes](https://mermaid-js.github.io/mermaid/#/./theming))
  * Different types of diagrams ([sequence diagrams](https://mermaid-js.github.io/mermaid/#/sequenceDiagram), [user journey diagrams](https://mermaid-js.github.io/mermaid/#/user-journey))
  * Adding links to "boxes." Here is how to add [interaction(https://mermaid-js.github.io/mermaid/#/flowchart?id=interaction)] to Mermaid diagrams.
* **Working with Tables**
  * This [Extended Syntax](https://www.markdownguide.org/extended-syntax/) guide has all kinds of goodies, including how to work with tables (just scroll down on the page to the section on *tables*).
  * [Markdown Table Generator](https://www.tablesgenerator.com/markdown_tables). This is a tool to create a table in Markdown. It will then generate the code you need to add to your Markdown document.
  * Adding pictures to tables. You can do this the same way you add images anywhere else in Markdown. You just add the picture *code* in whatever cell(s) you want.
  * Adding links in tables. You can do this the same way you add links to anything else in Markdown. You just add the link *code* in whatever cell(s) you want.
* **Collaboration on Markdown Documents**

  * StackEdit does not allow people to collaborate in real time on Markdown documents. However, there is another Markdown editor, [HackMD](https://hackmd.io/) that does allow you to create teams which can then collaborate in real time on Markdown documents. Here is a [document](https://hackmd.io/c/tutorials/%2Fs%2Ftutorials) with tutorials, including a section on collaboration.
* **Table of Contents in  Markdown Documents**
  * It is pretty straightforward to add a dynamic Table of Contents (TOC) ot your Markdown document, and many Markdown editors have tools for doing so. However, StackEdit is not able to handle this feature. However, there is another Markdown editor, [HackMD](https://hackmd.io/) that does allow you to easily create TOCs. [Here is an example](https://hackmd.io/@geraldardito/HJKYKpZzs/edit) of how this works.
* **Embedding Sounds in Markdown Documents**

  * It is pretty straightforward to embed audio into Markdown documents. To do this, you would borrow the <audio> tag from HTML. Here is an example and the associated code.


```html
<audio controls>
  <source src="https://bigsoundbank.com/UPLOAD/wav/0004.wav" type="audio/wav" />
</audio>
```

<audio controls>
  <source src="https://bigsoundbank.com/UPLOAD/wav/0004.wav" type="audio/wav" />
</audio>

The key parts are the link to the sound file. Here is a [great, free repository](https://bigsoundbank.com) of all kinds of sound files. The file  must end in a sound related extension (like wav, ogg, mp3). Then, you would update the "type" section to reflect the type of file you are using.

