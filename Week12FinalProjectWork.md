[TOC]

# Final Project - Weeks 12/13/14

<img src="https://images.unsplash.com/photo-1531403009284-440f080d1e12?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" style="zoom:100%;" />

***


## Final Projects

### Designing a Final Project

<img src="https://images.unsplash.com/photo-1581490278090-2249a0204d95?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80" style="zoom:57%;" />

The last chunk of work for this course will be a **final project**. 
In this project you can either choose something we have already worked on so far this semester and dive more deeply into it **OR** you can choose another area of learning technologies that you would like to explore for 2-3 weeks.

You can see the project requirements and rubric [here](https://docs.google.com/document/d/1XPaE36CtgFi5w_ARRhI4VbAsYSrfegOy-Hi0TdMMl58/edit?usp=sharing).

As you will see, your first task is to create a project proposal, which I will approve. Please do this via a shareable document, such as Google Docs or Office 365. If Google Docs, share via arditog@mville.edu. If Office 365, share via gerald.ardito@mville. In either case, please be sure to give me editing privileges.

### Working on Your Final Project

<img src="https://images.unsplash.com/photo-1492366254240-43affaefc3e3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=650&q=80" style="zoom:67%;" />

Once you have had your proposal approved by me, you are ready to get to work. Your work will be determined, at least to start, by the curriculum you designed for yourself in your proposal. 

Remember to make notes on your progress as you go. This will help you put together your class presentation.

### Final Project Resources

I have put together [this collection of resources](https://gitlab.com/gerald.ardito/teachingwithtech/-/blob/main/Final%20Project%20Resources.md) to support your work on your final projects. As you identify things that you find useful and that you'd like to share with your classmates, please send them along and I will add them to this page.

***
### Sharing Your Final Projects

<img src="https://images.unsplash.com/photo-1461354464878-ad92f492a5a0?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" style="zoom:67%;" />

In our last class, on **Tuesday, December 13**, you will share a brief (***7-10 minute***) presentation that will include your intentions for the project, what you learned, where you go next, and a reflection on the entire process. 

Note - after your presentation, you will share all project artifacts with me. This will comprise *turning in* this work.

***





